// SEC 4: DEFINED FUNCTIONS

  // defined function, using pattern matching
  def factorial(n: Int): Int = n match {
    case 0 | 1 => 1   // 0 or 1 gets mapped to 1
    case _ => n * factorial(n - 1)  // otherwise
  }

  // nested functions, and tail recursion
  def factorial2(n: Int): Int = {
    @annotation.tailrec  // checks if optimisation is possible
    def _fact(n: Int, acc: Int): Int = n match {
      case 0 | 1 => acc
      case _ => _fact (n - 1, n * acc)
    }
    _fact(n, 1)
  }

  println(factorial(6)) // 720
  println(factorial2(6)) // 720

  // currying.. curriedMult: Double => (Double => Double)
  def curriedMult(x: Double)(y: Double): Double = x*y
  val twice: Double => Double = curriedMult(2)
  val cm: Double => Double => Double = curriedMult(_) // D => (D => D)
  println(twice(10))
  println(cm(2)(10)) // println(curriedMult(2)(10))