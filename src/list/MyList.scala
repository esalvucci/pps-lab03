package list

import java.util.function.Predicate

import scala.annotation.tailrec

// A generic linkedlist
sealed trait MyList[+A]

// a companion object (i.e., module) for List
object MyList extends App {
  case class Cons[A](head: A, tail: MyList[A]) extends MyList[A]
  case class Nil[A]() extends MyList[A]

  def length[A](l: MyList[A]): Int = {
    @tailrec
    def loop(s: MyList[A], accumulator: Int): Int = s match {
      case Nil() => accumulator
      case Cons(_, t) => loop(t, accumulator + 1)
    }
    loop(l, 0)
  }

  def sum(l: MyList[Int]): Int = {
    @tailrec
    def loop(s: MyList[Int], accumulator: Int): Int = s match {
      case Nil() => accumulator
      case Cons(h, t) => loop(t, accumulator+h)
    }
    loop(l, 0)
  }

  def append[A <: C, B <: C, C](l1: MyList[A], l2: MyList[B]): MyList[C] = (l1, l2) match {
    case (Cons(h, t), l2) => Cons[C](h, append(t, l2))
    case (l1, Cons(h, t)) => Cons[C](h, append(l1, t))
    case _ => Nil()
  }

  def drop[A](l: MyList[A], n: Int): MyList[A] = {
    @tailrec
    def loop(s: MyList[A], n: Int): MyList[A] = n match {
      case n if n <= 0 => s
      case _ => s match {
        case Nil() => Nil()
        case Cons(_, t) => loop(t, n - 1)
      }
    }
    loop(l, n)
  }

  def dropWhile[A](list: MyList[A])(predicate: A => Boolean): MyList[A] = {

    def loop(s: MyList[A], predicate: A => Boolean): MyList[A] = s match {
      case Cons(h, t) => if (predicate(h)) loop(t, predicate) else Cons(h, loop(t, predicate))
      case Nil() =>  s
    }
    loop(list, predicate)
  }

  /**
    * Builds a new collection by applying a function to all elements of this list.
    */
  def map[A, B](l: MyList[A])(f: A => B): MyList[B] = {
    def loop(s: MyList[A], g: A => B): MyList[B] = s match {
      case Nil() => Nil()
      case Cons(h, t) => Cons(g(h), loop(t, g))
    }
    loop(l, f)

  }

  /**
   * Selects all elements of this traversable collection which satisfy a predicate.
   */
  def filter[A](l: MyList[A])(predicate: A => Boolean): MyList[A] = {

    def loop(s: MyList[A]): MyList[A] = s match {
      case Nil() => s
      case Cons(h, t) =>
        if (predicate(h)) Cons(h, loop(t))
        else loop(t)
    }
    loop(l)
  }

  def setHead[A](list: MyList[A], head: A): MyList[A] = list match {
    case Cons(h, t) => Cons(head, t)
    case Nil() => list
  }

/*
  def max(l: MyList[Int]): Option[Int] = {
    @tailrec
    def loop(l: MyList[Int], previousMax: Option[Int]): Option[Int] = l match {
      case Cons(h, t) =>
        if(h > previousMax.get) loop(t, Option(h))
        else loop(t, previousMax)
      case _ => previousMax
    }

    loop(l, Option(0))
  }
*/

  System.out.println(length(Cons(10, Cons(20, Cons(30, Nil())))))
  System.out.println(length(Nil()))

  System.out.println(sum(Cons(10, Cons(20, Cons(30, Nil())))))
  System.out.println(sum(Nil()))

  System.out.println(drop(Cons(10, Cons(20, Cons(30, Nil()))),2))
  System.out.println(drop(Cons(10, Cons(20, Cons(30, Nil()))),5))

  System.out.println(map(Cons(10, Cons(20, Nil())))(_+1)) // Cons(11, Cons(21, Nil()))
  System.out.println(map(Cons(10, Cons(20, Nil())))(":"+_+":"))   // Cons(":10:", Cons(":20:", Nil()))

  System.out.println("Filter:")
  System.out.println(filter(Cons(10, Cons(20, Nil())))(_>15))
  System.out.println(filter(Cons("a", Cons("bb", Cons("ccc", Nil()))))( _.length <=2))
//  System.out.println(max(Nil()))
//  System.out.println(max(Cons(10, Cons(25, Cons(20, Nil())))))

  System.out.println(setHead(Cons(10, Cons(20, Cons(30, Nil()))), 50))
  System.out.println(dropWhile(Cons(10, Cons(21, Cons(30, Nil()))))(_ % 2 == 0))
  System.out.println(dropWhile(Cons(10, Cons(21, Cons(30, Nil()))))(_ % 2 != 0))


}